# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Db.Repo.insert!(%FaithfulWord.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Elijah.Repo
alias Elijah.Timeline.MediaItem

import Ecto.Query
require Logger


Logger.debug("Application.get_env #{Application.get_env(:db, :env)}")
# Create Admin in dev or if we're running image locally
# if Application.get_env(:db, :env) == :dev do
# Logger.warn("API is running in dev mode. Inserting default user admin@faithfulword.app")

# No need to warn if already exists

# Update all existing mediaitems with their hashIds
Elijah.Repo.all(from mi in MediaItem)
|> Enum.map(&MediaItem.changeset_generate_hash_id/1)
|> Enum.map(&Elijah.Repo.update/1)
