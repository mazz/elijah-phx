defmodule Elijah.Repo.Migrations.CreateMediaitems do
  use Ecto.Migration

  def change do
    create table(:mediaitems) do
      add :uuid, :uuid
      add :filepath, :string
      add :title, :string
      add :description, :string
      add :duration, :integer
      add :width, :integer
      add :height, :integer
      add :tags, :string
      add :view_count, :integer
      add :like_count, :integer
      add :dislike_count, :integer
      add :webpage_url, :string
      add :uploader, :string
      add :upload_date, :utc_datetime

      timestamps()
    end

  end
end
