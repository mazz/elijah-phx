defmodule Elijah.Repo.Migrations.CreateMediaitemsSearch do
  use Ecto.Migration

  def change do
    execute("""
    CREATE EXTENSION IF NOT EXISTS unaccent
    """)

    execute("""
    CREATE EXTENSION IF NOT EXISTS pg_trgm
    """)

    execute("""
    CREATE MATERIALIZED VIEW media_items_search AS
    SELECT  mediaitems.id AS id,
            mediaitems.title AS title,
            mediaitems.tags AS tags,
            mediaitems.uploader AS uploader,
            (
              setweight(to_tsvector('english', COALESCE(title,'')), 'A') ||
              setweight(to_tsvector('english', COALESCE(tags,'')), 'B') ||
              setweight(to_tsvector('english', COALESCE(uploader,'')), 'C')
            )
            AS document
     FROM mediaitems;
    """)

    # to support full-text searches
    create index("media_items_search", ["document"], using: :gin)

    # to support substring title matches with ILIKE
    execute(
      "CREATE INDEX media_items_search_title_trgm_index ON media_items_search USING gin (title gin_trgm_ops)"
    )

    # to support updating CONCURRENTLY
    create unique_index("media_items_search", [:id])

    flush()
  end
end

