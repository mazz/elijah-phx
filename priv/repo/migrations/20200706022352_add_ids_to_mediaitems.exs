defmodule Elijah.Repo.Migrations.AddIdsToMediaitems do
  use Ecto.Migration

  def change do
    alter table(:mediaitems) do
      add(:original_youtube_id, :string, null: false)
      add(:skylink_id, :string)
      # A size of 10 allows us to go up to 100_000_000_000_000 media items
      add(:hash_id, :string)
    end

    create(unique_index(:mediaitems, [:original_youtube_id]))
    create(unique_index(:mediaitems, [:skylink_id]))
    create(unique_index(:mediaitems, [:hash_id]))
  end
end
