defmodule ElijahWeb.MediaItemLiveTest do
  use ElijahWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Elijah.Timeline

  @create_attrs %{description: "some description", dislike_count: 42, duration: 42, filepath: "some filepath", height: 42, like_count: 42, tags: "some tags", title: "some title", upload_date: "2010-04-17T14:00:00Z", uploader: "some uploader", uuid: "7488a646-e31f-11e4-aace-600308960662", view_count: 42, webpage_url: "some webpage_url", width: 42}
  @update_attrs %{description: "some updated description", dislike_count: 43, duration: 43, filepath: "some updated filepath", height: 43, like_count: 43, tags: "some updated tags", title: "some updated title", upload_date: "2011-05-18T15:01:01Z", uploader: "some updated uploader", uuid: "7488a646-e31f-11e4-aace-600308960668", view_count: 43, webpage_url: "some updated webpage_url", width: 43}
  @invalid_attrs %{description: nil, dislike_count: nil, duration: nil, filepath: nil, height: nil, like_count: nil, tags: nil, title: nil, upload_date: nil, uploader: nil, uuid: nil, view_count: nil, webpage_url: nil, width: nil}

  defp fixture(:media_item) do
    {:ok, media_item} = Timeline.create_media_item(@create_attrs)
    media_item
  end

  defp create_media_item(_) do
    media_item = fixture(:media_item)
    %{media_item: media_item}
  end

  describe "Index" do
    setup [:create_media_item]

    test "lists all mediaitems", %{conn: conn, media_item: media_item} do
      {:ok, _index_live, html} = live(conn, Routes.media_item_index_path(conn, :index))

      assert html =~ "Listing Mediaitems"
      assert html =~ media_item.description
    end

    test "saves new media_item", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.media_item_index_path(conn, :index))

      assert index_live |> element("a", "New Media item") |> render_click() =~
               "New Media item"

      assert_patch(index_live, Routes.media_item_index_path(conn, :new))

      assert index_live
             |> form("#media_item-form", media_item: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#media_item-form", media_item: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.media_item_index_path(conn, :index))

      assert html =~ "Media item created successfully"
      assert html =~ "some description"
    end

    test "updates media_item in listing", %{conn: conn, media_item: media_item} do
      {:ok, index_live, _html} = live(conn, Routes.media_item_index_path(conn, :index))

      assert index_live |> element("#media_item-#{media_item.id} a", "Edit") |> render_click() =~
               "Edit Media item"

      assert_patch(index_live, Routes.media_item_index_path(conn, :edit, media_item))

      assert index_live
             |> form("#media_item-form", media_item: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#media_item-form", media_item: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.media_item_index_path(conn, :index))

      assert html =~ "Media item updated successfully"
      assert html =~ "some updated description"
    end

    test "deletes media_item in listing", %{conn: conn, media_item: media_item} do
      {:ok, index_live, _html} = live(conn, Routes.media_item_index_path(conn, :index))

      assert index_live |> element("#media_item-#{media_item.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#media_item-#{media_item.id}")
    end
  end

  describe "Show" do
    setup [:create_media_item]

    test "displays media_item", %{conn: conn, media_item: media_item} do
      {:ok, _show_live, html} = live(conn, Routes.media_item_show_path(conn, :show, media_item))

      assert html =~ "Show Media item"
      assert html =~ media_item.description
    end

    test "updates media_item within modal", %{conn: conn, media_item: media_item} do
      {:ok, show_live, _html} = live(conn, Routes.media_item_show_path(conn, :show, media_item))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Media item"

      assert_patch(show_live, Routes.media_item_show_path(conn, :edit, media_item))

      assert show_live
             |> form("#media_item-form", media_item: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#media_item-form", media_item: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.media_item_show_path(conn, :show, media_item))

      assert html =~ "Media item updated successfully"
      assert html =~ "some updated description"
    end
  end
end
