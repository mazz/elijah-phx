defmodule Elijah.TimelineTest do
  use Elijah.DataCase

  alias Elijah.Timeline

  describe "mediaitems" do
    alias Elijah.Timeline.MediaItem

    @valid_attrs %{description: "some description", dislike_count: 42, duration: 42, filepath: "some filepath", height: 42, like_count: 42, tags: "some tags", title: "some title", upload_date: "2010-04-17T14:00:00Z", uploader: "some uploader", uuid: "7488a646-e31f-11e4-aace-600308960662", view_count: 42, webpage_url: "some webpage_url", width: 42}
    @update_attrs %{description: "some updated description", dislike_count: 43, duration: 43, filepath: "some updated filepath", height: 43, like_count: 43, tags: "some updated tags", title: "some updated title", upload_date: "2011-05-18T15:01:01Z", uploader: "some updated uploader", uuid: "7488a646-e31f-11e4-aace-600308960668", view_count: 43, webpage_url: "some updated webpage_url", width: 43}
    @invalid_attrs %{description: nil, dislike_count: nil, duration: nil, filepath: nil, height: nil, like_count: nil, tags: nil, title: nil, upload_date: nil, uploader: nil, uuid: nil, view_count: nil, webpage_url: nil, width: nil}

    def media_item_fixture(attrs \\ %{}) do
      {:ok, media_item} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Timeline.create_media_item()

      media_item
    end

    test "list_mediaitems/0 returns all mediaitems" do
      media_item = media_item_fixture()
      assert Timeline.list_mediaitems() == [media_item]
    end

    test "get_media_item!/1 returns the media_item with given id" do
      media_item = media_item_fixture()
      assert Timeline.get_media_item!(media_item.id) == media_item
    end

    test "create_media_item/1 with valid data creates a media_item" do
      assert {:ok, %MediaItem{} = media_item} = Timeline.create_media_item(@valid_attrs)
      assert media_item.description == "some description"
      assert media_item.dislike_count == 42
      assert media_item.duration == 42
      assert media_item.filepath == "some filepath"
      assert media_item.height == 42
      assert media_item.like_count == 42
      assert media_item.tags == "some tags"
      assert media_item.title == "some title"
      assert media_item.upload_date == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert media_item.uploader == "some uploader"
      assert media_item.uuid == "7488a646-e31f-11e4-aace-600308960662"
      assert media_item.view_count == 42
      assert media_item.webpage_url == "some webpage_url"
      assert media_item.width == 42
    end

    test "create_media_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Timeline.create_media_item(@invalid_attrs)
    end

    test "update_media_item/2 with valid data updates the media_item" do
      media_item = media_item_fixture()
      assert {:ok, %MediaItem{} = media_item} = Timeline.update_media_item(media_item, @update_attrs)
      assert media_item.description == "some updated description"
      assert media_item.dislike_count == 43
      assert media_item.duration == 43
      assert media_item.filepath == "some updated filepath"
      assert media_item.height == 43
      assert media_item.like_count == 43
      assert media_item.tags == "some updated tags"
      assert media_item.title == "some updated title"
      assert media_item.upload_date == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert media_item.uploader == "some updated uploader"
      assert media_item.uuid == "7488a646-e31f-11e4-aace-600308960668"
      assert media_item.view_count == 43
      assert media_item.webpage_url == "some updated webpage_url"
      assert media_item.width == 43
    end

    test "update_media_item/2 with invalid data returns error changeset" do
      media_item = media_item_fixture()
      assert {:error, %Ecto.Changeset{}} = Timeline.update_media_item(media_item, @invalid_attrs)
      assert media_item == Timeline.get_media_item!(media_item.id)
    end

    test "delete_media_item/1 deletes the media_item" do
      media_item = media_item_fixture()
      assert {:ok, %MediaItem{}} = Timeline.delete_media_item(media_item)
      assert_raise Ecto.NoResultsError, fn -> Timeline.get_media_item!(media_item.id) end
    end

    test "change_media_item/1 returns a media_item changeset" do
      media_item = media_item_fixture()
      assert %Ecto.Changeset{} = Timeline.change_media_item(media_item)
    end
  end
end
