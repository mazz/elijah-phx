defmodule Elijah.Timeline.MediaItem do
  use Ecto.Schema
  import Ecto.Changeset
  alias Elijah.DbType.MediaItemHashId

  schema "mediaitems" do
    field :description, :string
    field :dislike_count, :integer
    field :duration, :integer
    field :filepath, :string
    field :height, :integer
    field :like_count, :integer
    field :tags, :string
    field :title, :string
    field :upload_date, :utc_datetime
    field :uploader, :string
    field :uuid, Ecto.UUID
    field :view_count, :integer
    field :webpage_url, :string
    field :width, :integer
    field :original_youtube_id, :string
    field :skylink_id, :string
    field :hash_id, :string

    timestamps()
  end

  @doc false
  def changeset(media_item, attrs) do
    media_item
    |> cast(attrs, [:uuid, :filepath, :title, :description, :duration, :width, :height, :tags, :view_count, :like_count, :dislike_count, :webpage_url, :uploader, :upload_date, :original_youtube_id, :hash_id])
    |> validate_required([:uuid, :filepath, :title, :description, :duration, :width, :height, :tags, :view_count, :like_count, :dislike_count, :webpage_url, :uploader, :upload_date, :original_youtube_id])
  end

  @doc """
  Generate hash ID for media items

  ## Examples

      iex> Db.Schema.MediaItem.changeset_generate_hash_id(%Db.Schema.Video{id: 42, hash_id: nil})
      #Ecto.Changeset<action: nil, changes: %{hash_id: \"4VyJ\"}, errors: [], data: #Db.Schema.Video<>, valid?: true>
  """
  def changeset_generate_hash_id(media_item) do
    change(media_item, hash_id: MediaItemHashId.encode(media_item.id))
  end
end
