defmodule Elijah.Timeline do
  @moduledoc """
  The Timeline context.
  """

  import Ecto.Query, warn: false
  alias Elijah.Repo

  alias Elijah.Timeline.MediaItem

  @doc """
  Returns the list of mediaitems.

  ## Examples

      iex> list_mediaitems()
      [%MediaItem{}, ...]

  """
  def list_mediaitems do
    Repo.all(MediaItem)
  end

  @doc """
  Gets a single media_item.

  Raises `Ecto.NoResultsError` if the Media item does not exist.

  ## Examples

      iex> get_media_item!(123)
      %MediaItem{}

      iex> get_media_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_media_item!(id), do: Repo.get!(MediaItem, id)

  @doc """
  Creates a media_item.

  ## Examples

      iex> create_media_item(%{field: value})
      {:ok, %MediaItem{}}

      iex> create_media_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_media_item(attrs \\ %{}) do
    %MediaItem{}
    |> MediaItem.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a media_item.

  ## Examples

      iex> update_media_item(media_item, %{field: new_value})
      {:ok, %MediaItem{}}

      iex> update_media_item(media_item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_media_item(%MediaItem{} = media_item, attrs) do
    media_item
    |> MediaItem.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a media_item.

  ## Examples

      iex> delete_media_item(media_item)
      {:ok, %MediaItem{}}

      iex> delete_media_item(media_item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_media_item(%MediaItem{} = media_item) do
    Repo.delete(media_item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking media_item changes.

  ## Examples

      iex> change_media_item(media_item)
      %Ecto.Changeset{data: %MediaItem{}}

  """
  def change_media_item(%MediaItem{} = media_item, attrs \\ %{}) do
    MediaItem.changeset(media_item, attrs)
  end
end
