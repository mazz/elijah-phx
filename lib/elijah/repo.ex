defmodule Elijah.Repo do
  use Ecto.Repo,
    otp_app: :elijah,
    adapter: Ecto.Adapters.Postgres

  use Scrivener, page_size: 50
end
