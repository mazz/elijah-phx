defmodule ElijahWeb.MediaItemLive.FormComponent do
  use ElijahWeb, :live_component

  alias Elijah.Timeline

  @impl true
  def update(%{media_item: media_item} = assigns, socket) do
    changeset = Timeline.change_media_item(media_item)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"media_item" => media_item_params}, socket) do
    changeset =
      socket.assigns.media_item
      |> Timeline.change_media_item(media_item_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"media_item" => media_item_params}, socket) do
    save_media_item(socket, socket.assigns.action, media_item_params)
  end

  defp save_media_item(socket, :edit, media_item_params) do
    case Timeline.update_media_item(socket.assigns.media_item, media_item_params) do
      {:ok, _media_item} ->
        {:noreply,
         socket
         |> put_flash(:info, "Media item updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_media_item(socket, :new, media_item_params) do
    case Timeline.create_media_item(media_item_params) do
      {:ok, _media_item} ->
        {:noreply,
         socket
         |> put_flash(:info, "Media item created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
