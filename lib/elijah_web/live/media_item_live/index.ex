defmodule ElijahWeb.MediaItemLive.Index do
  use ElijahWeb, :live_view

  alias Elijah.Timeline
  alias Elijah.Timeline.MediaItem

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :mediaitems, list_mediaitems())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Media item")
    |> assign(:media_item, Timeline.get_media_item!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Media item")
    |> assign(:media_item, %MediaItem{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Mediaitems")
    |> assign(:media_item, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    media_item = Timeline.get_media_item!(id)
    {:ok, _} = Timeline.delete_media_item(media_item)

    {:noreply, assign(socket, :mediaitems, list_mediaitems())}
  end

  defp list_mediaitems do
    Timeline.list_mediaitems()
  end
end
