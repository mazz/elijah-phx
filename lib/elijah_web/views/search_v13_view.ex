defmodule ElijahWeb.SearchV13View do
    use ElijahWeb, :view
    alias ElijahWeb.SearchV13View

    def render("searchv13.json", %{search_v13: search_v13, api_version: api_version}) do
      %{
        result: render_many(search_v13, SearchV13View, "search_v13.json"),
        pageNumber: search_v13.page_number,
        pageSize: search_v13.page_size,
        status: "success",
        totalEntries: search_v13.total_entries,
        totalPages: search_v13.total_pages,
        version: api_version
      }
  
      # %{data: render_many(search_v13, SearchV13View, "search_v13.json")}
    end
  
    def render("show.json", %{search_v13: search_v13}) do
      %{data: render_one(search_v13, SearchV13View, "search_v13.json")}
    end

    def render("search_v13.json", %{search_v13: search_v13}) do
      %{
        description: search_v13.description,
        dislike_count: search_v13.dislike_count,
        duration: search_v13.duration,
        filepath: search_v13.filepath,
        height: search_v13.height,
        like_count: search_v13.like_count,
        tags: search_v13.tags,
        title: search_v13.title,
        upload_date: search_v13.upload_date,
        uploader: search_v13.uploader,
        uuid: search_v13.uuid,
        view_count: search_v13.view_count,
        webpage_url: search_v13.webpage_url,
        width: search_v13.width,
        original_youtube_id: search_v13.original_youtube_id,
        skylink_id: search_v13.skylink_id,
        hash_id: search_v13.hash_id,
        insertedAt: search_v13.inserted_at,
        updatedAt: search_v13.updated_at
      }
    end
  end
