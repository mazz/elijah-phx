defmodule ElijahWeb.SearchController do
    use ElijahWeb, :controller
  
    alias ElijahWeb.V13
    alias ElijahWeb.ErrorView
    alias ElijahWeb.SearchV13View
  
    require Logger
  
    action_fallback ElijahWeb.FallbackController
  
    def searchv13(
          conn,
          params = %{"query" => query_string, "offset" => offset, "limit" => limit}
        ) do
      # optional params  
      V13.search(
        query_string,
        offset,
        limit
        # media_category,
        # playlist_uuid,
        # channel_uuid,
        # published_after,
        # updated_after,
        # presented_after
      )
      |> case do
        nil ->
          put_status(conn, 403)
          |> put_view(ErrorView)
          |> render("403.json", %{message: "something bad happened"})
  
        search_v13 ->
          Logger.debug("search_v13 #{inspect(%{attributes: search_v13})}")
  
          Enum.at(conn.path_info, 0)
          |> case do
            api_version ->
              api_version = String.trim_leading(api_version, "v")
  
              conn
              |> put_view(SearchV13View)
              |> render("searchv13.json", %{search_v13: search_v13, api_version: api_version})
          end
      end
    end
  end
  