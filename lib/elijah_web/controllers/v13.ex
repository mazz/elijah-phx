defmodule ElijahWeb.V13 do
    import Ecto.Query, warn: false
    alias Elijah.Repo
  
    alias Elijah.Timeline.{MediaItem}
  
    alias ElijahWeb.MediaItemsSearch
  
    require Ecto.Query
    require Logger
    require DateTime
  



    # def media_item_by_hash_id(hash_id) do
    #   query =
    #     from(mi in MediaItem,
    #       where: mi.hash_id == ^hash_id,
    #       select: %{
    #         description: mi.description,
    #         dislike_count: mi.dislike_count,
    #         duration: mi.duration,
    #         filepath: mi.filepath,
    #         height: mi.height,
    #         like_count: pl.like_count,
    #         path: mi.path,
    #         content_provider_link: mi.content_provider_link,
    #         ipfs_link: mi.ipfs_link,
    #         language_id: mi.language_id,
    #         presenter_name: mi.presenter_name,
    #         source_material: mi.source_material,
    #         tags: mi.tags,
    #         # small_thumbnail_path: mi.small_thumbnail_path,
    #         # med_thumbnail_path: mi.med_thumbnail_path,
    #         # large_thumbnail_path: mi.large_thumbnail_path,
    #         inserted_at: mi.inserted_at,
    #         updated_at: mi.updated_at,
    #         # media_category: mi.media_category,
    #         # presented_at: mi.presented_at,
    #         # published_at: mi.published_at,
    #         # hash_id: mi.hash_id,
    #         # playlist_uuid: pl.uuid,
    #         duration: mi.duration
    #       }
    #     )
  
    #   query
    #   |> Repo.one()
    # end
  

    def search(
          query_string,
          offset \\ 0,
          limit \\ 0
        #   media_category,
        #   playlist_uuid,
        #   channel_uuid,
        #   published_after,
        #   updated_after,
        #   presented_after
        ) do
  
      # jan 2019: 1546527751
      # jan 2020: 1578063751
      # apr 29 2019: 1556550151
      # April 26, 2013 3:02:31 PM: 1366988551
      # May 3, 2017 11:16:55 PM: 1493853415
      # May 3, 2018 11:16:55 PM: 1525389415
      # preaching channel uuid f467f75c-937a-46a3-a21f-880bb9777408
      # music channel uuid 52f758d2-ce64-4ffd-8d3c-77f598003ee1
  
      MediaItemsSearch.run(Ecto.Query.from(mi in MediaItem), query_string)
      |> Repo.paginate(page: offset, page_size: limit)

    #   MediaItemsSearch.run(query, query_string)
    #   |> Repo.paginate(page: offset, page_size: limit)
    end
  end
