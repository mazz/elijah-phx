defmodule ElijahWeb.Router do
  use ElijahWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ElijahWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", ElijahWeb do
    pipe_through :browser

    live "/", PageLive, :index
    live "/mediaitems", MediaItemLive.Index, :index
    live "/mediaitems/new", MediaItemLive.Index, :new
    live "/mediaitems/:id/edit", MediaItemLive.Index, :edit

    live "/mediaitems/:id", MediaItemLive.Show, :show
    live "/mediaitems/:id/show/edit", MediaItemLive.Show, :edit

    scope "/search" do
      post "/", SearchController, :searchv13
    end

  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ElijahWeb do
    pipe_through :api

    scope "/search" do
      post "/", SearchController, :searchv13
    end

  end
  



  # Other scopes may use custom stacks.
  # scope "/api", ElijahWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: ElijahWeb.Telemetry
    end
  end
end
